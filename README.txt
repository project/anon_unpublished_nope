Returns a 404 status when a anonymous user tries to access unpublished content.
